# IconRequest - Icon Request Tool

With this app you can share (via email, or other protocols like Owncloud, NFC, Bluetooth, IM, etc...)
the data for adding the icons missing in your icon pack.
It just collects the app title, package name, activity name and the icon file without any theme of the apps you select.

## Preview



# Installation
<a href="https://f-droid.org/packages/org.kaiserdragon.iconrequest">
    <img src="https://f-droid.org/badge/get-it-on.png"
    alt="Get it on F-Droid" height="80">
</a>
<a href="https://gitlab.com/xphnx/icon-request-tool/-/jobs/artifacts/master/download?job=build">
    
    Download the latest build
</a>

These permissions are needed to be able to create the zip files.

## License

[<img src="https://www.gnu.org/graphics/gplv3-127x51.png" />](https://www.gnu.org/licenses/gpl-3.0.html)
